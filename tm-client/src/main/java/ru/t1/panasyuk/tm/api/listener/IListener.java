package ru.t1.panasyuk.tm.api.listener;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.event.ConsoleEvent;

public interface IListener {

    void handler(@NotNull ConsoleEvent consoleEvent);

    @Nullable
    String getArgument();

    @Nullable
    String getDescription();

    @NotNull
    String getName();

    @Nullable
    Role[] getRoles();

}