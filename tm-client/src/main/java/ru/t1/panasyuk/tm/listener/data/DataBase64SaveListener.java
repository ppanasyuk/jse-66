package ru.t1.panasyuk.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.data.DataBase64SaveRequest;
import ru.t1.panasyuk.tm.event.ConsoleEvent;

@Component
public final class DataBase64SaveListener extends AbstractDataListener {

    @NotNull
    private static final String DESCRIPTION = "Save data to base64 file.";

    @NotNull
    private static final String NAME = "data-save-base64";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBase64SaveListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA BASE64 SAVE]");
        @NotNull final DataBase64SaveRequest request = new DataBase64SaveRequest(getToken());
        domainEndpoint.saveDataBase64(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}