package ru.t1.panasyuk.tm.dto.request.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class DataBackupSaveRequest extends AbstractUserRequest {

    public DataBackupSaveRequest(@Nullable final String token) {
        super(token);
    }

}