package ru.t1.panasyuk.tm.service.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.panasyuk.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.panasyuk.tm.constant.FieldConst;
import ru.t1.panasyuk.tm.exception.entity.ProjectNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.TaskNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;
import ru.t1.panasyuk.tm.exception.field.ProjectIdEmptyException;
import ru.t1.panasyuk.tm.exception.field.TaskIdEmptyException;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;
import ru.t1.panasyuk.tm.repository.dto.ProjectDtoRepository;
import ru.t1.panasyuk.tm.repository.dto.TaskDtoRepository;

import java.util.List;

@Service
public class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    @Autowired
    protected ApplicationContext context;

    @Getter
    @NotNull
    @Autowired
    private ProjectDtoRepository projectRepository;

    @Getter
    @NotNull
    @Autowired
    protected TaskDtoRepository taskRepository;

    @NotNull
    @Override
    @Transactional
    public TaskDTO bindTaskToProject(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final TaskDTO task;
        @NotNull final ProjectDtoRepository projectRepository = getProjectRepository();
        @NotNull final TaskDtoRepository taskRepository = getTaskRepository();
        boolean isExist = projectRepository.findFirstByUserIdAndId(userId, projectId) != null;
        if (!isExist) throw new ProjectNotFoundException();
        task = taskRepository.findFirstByUserIdAndId(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        taskRepository.save(task);
        return task;
    }

    @Override
    @Transactional
    public ProjectDTO removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable ProjectDTO project;
        @NotNull final ProjectDtoRepository projectRepository = getProjectRepository();
        @NotNull final TaskDtoRepository taskRepository = getTaskRepository();
        project = projectRepository.findFirstByUserIdAndId(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        taskRepository.deleteByUserIdAndProjectId(userId, projectId);
        projectRepository.delete(project);
        return project;
    }

    @Override
    @Transactional
    public ProjectDTO removeProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final ProjectDTO project;
        @NotNull final ProjectDtoRepository projectRepository = getProjectRepository();
        if (index > projectRepository.countByUserId(userId)) throw new IndexIncorrectException();
        @NotNull final Pageable pageable = PageRequest.of(index - 1, 1);
        project = projectRepository
                .findByIndex(userId, pageable)
                .stream()
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
        removeProjectById(userId, project.getId());
        return project;
    }

    @Override
    @Transactional
    public void clearProjects(@NotNull final String userId) {
        @Nullable final List<ProjectDTO> projects;
        @NotNull final ProjectDtoRepository projectRepository = getProjectRepository();
        @NotNull final Sort sort = Sort.by(Sort.Direction.DESC, FieldConst.FIELD_CREATED);
        projects = projectRepository.findByUserId(userId, sort);
        if (projects == null) return;
        for (@NotNull final ProjectDTO project : projects) removeProjectById(userId, project.getId());
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO unbindTaskFromProject(
            @NotNull final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final TaskDTO task;
        @NotNull final ProjectDtoRepository projectRepository = getProjectRepository();
        @NotNull final TaskDtoRepository taskRepository = getTaskRepository();
        boolean isExist = projectRepository.findFirstByUserIdAndId(userId, projectId) != null;
        if (!isExist) throw new ProjectNotFoundException();
        task = taskRepository.findFirstByUserIdAndId(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        taskRepository.save(task);
        return task;
    }

}