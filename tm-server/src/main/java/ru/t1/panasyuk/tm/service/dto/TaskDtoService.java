package ru.t1.panasyuk.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.panasyuk.tm.api.service.dto.ITaskDtoService;
import ru.t1.panasyuk.tm.comparator.NameComparator;
import ru.t1.panasyuk.tm.comparator.StatusComparator;
import ru.t1.panasyuk.tm.constant.FieldConst;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.TaskNotFoundException;
import ru.t1.panasyuk.tm.exception.field.*;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;
import ru.t1.panasyuk.tm.repository.dto.TaskDtoRepository;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public final class TaskDtoService extends AbstractUserOwnedDtoService<TaskDTO, TaskDtoRepository>
        implements ITaskDtoService {

    @NotNull
    @Override
    @Transactional
    public TaskDTO changeTaskStatusById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        update(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO changeTaskStatusByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (index == null || index <= 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        update(task);
        return task;
    }

    @Override
    @Transactional
    public void clear(@NotNull final String userId) {
        @NotNull final TaskDtoRepository repository = getRepository();
        repository.deleteByUserId(userId);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        long result;
        @NotNull final TaskDtoRepository repository = getRepository();
        result = repository.countByUserId(userId);
        return result;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(
            @NotNull final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return add(userId, task);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setUserId(userId);
        return add(userId, task);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @Nullable final String id) {
        boolean result;
        @NotNull final TaskDtoRepository repository = getRepository();
        result = repository.findFirstByUserIdAndId(userId, id) != null;
        return result;
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@NotNull final String userId, @Nullable final Comparator comparator) {
        @Nullable List<TaskDTO> models;
        @NotNull final TaskDtoRepository repository = getRepository();
        @NotNull String field = FieldConst.FIELD_CREATED;
        if (comparator == NameComparator.INSTANCE) field = FieldConst.FIELD_NAME;
        else if (comparator == StatusComparator.INSTANCE) field = FieldConst.FIELD_STATUS;
        @NotNull org.springframework.data.domain.Sort sort = org.springframework.data.domain.Sort.by(
                org.springframework.data.domain.Sort.Direction.DESC,
                field);
        models = repository.findByUserId(userId, sort);
        return models;
    }

    @Nullable
    @Override
    @SuppressWarnings("unchecked")
    public List<TaskDTO> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        final Comparator<TaskDTO> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @Nullable final List<TaskDTO> tasks;
        @NotNull final TaskDtoRepository repository = getRepository();
        tasks = repository.findByUserIdAndProjectIdOrderByCreatedDesc(userId, projectId);
        return tasks;
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@NotNull final String userId) {
        @Nullable final List<TaskDTO> models;
        @NotNull final TaskDtoRepository repository = getRepository();
        @NotNull final org.springframework.data.domain.Sort sort =
                org.springframework.data.domain.Sort.by(org.springframework.data.domain.Sort.Direction.DESC, "created");
        models = repository.findByUserId(userId, sort);
        return models;
    }

    @Nullable
    @Override
    public TaskDTO findOneByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final TaskDTO model;
        @NotNull final TaskDtoRepository repository = getRepository();
        @NotNull final Pageable pageable = PageRequest.of(index - 1, 1);
        model = repository
                .findByIndex(userId, pageable)
                .stream()
                .findFirst()
                .orElse(null);
        return model;
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@NotNull final String userId, @Nullable final String id) {
        if (id == null) return null;
        @Nullable final TaskDTO model;
        @NotNull final TaskDtoRepository repository = getRepository();
        model = repository.findFirstByUserIdAndId(userId, id);
        return model;
    }

    @Override
    @Transactional
    public void removeAllByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        @NotNull final TaskDtoRepository repository = getRepository();
        repository.deleteByUserIdAndProjectId(userId, projectId);
    }

    @Nullable
    @Override
    @Transactional
    public TaskDTO removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final TaskDTO removedModel;
        @NotNull final TaskDtoRepository repository = getRepository();
        @NotNull final Pageable pageable = PageRequest.of(index - 1, 1);
        removedModel = repository
                .findByIndex(userId, pageable)
                .stream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
        repository.delete(removedModel);
        return removedModel;
    }

    @Nullable
    @Override
    @Transactional
    public TaskDTO removeById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO removedModel;
        @NotNull final TaskDtoRepository repository = getRepository();
        removedModel = repository.findFirstByUserIdAndId(userId, id);
        if (removedModel == null) throw new EntityNotFoundException();
        repository.delete(removedModel);
        return removedModel;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO updateById(
            @NotNull final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final TaskDTO task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        update(task);
        return task;
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO updateByIndex(
            @NotNull final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (index == null || index <= 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final TaskDTO task = findOneByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        update(task);
        return task;
    }

}