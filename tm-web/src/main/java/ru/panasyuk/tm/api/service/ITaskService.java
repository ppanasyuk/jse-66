package ru.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.panasyuk.tm.model.Task;

import java.util.List;

public interface ITaskService {

    @Nullable
    List<Task> findAll();

    @NotNull
    Task save(@NotNull Task project);

    public void saveAll(@NotNull List<Task> projects);

    @Nullable
    Task findById(@NotNull String id);

    boolean existById(@NotNull String id);

    long count();

    void deleteById(@NotNull String id);

    void delete(@NotNull Task project);

    void clear(@NotNull List<Task> projects);

    void clear();

}