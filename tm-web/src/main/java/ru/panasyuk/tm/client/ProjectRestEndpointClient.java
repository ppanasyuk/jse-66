package ru.panasyuk.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.panasyuk.tm.model.Project;

@FeignClient
public interface ProjectRestEndpointClient {

    String BASE_URL = "http://localhost:8080/api/projects";

    static ProjectRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(ProjectRestEndpointClient.class, BASE_URL);
    }

    @GetMapping("findById/{id}")
    Project findById(@PathVariable("id") String id);

    @PostMapping("deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @PostMapping("delete")
    void delete(@RequestBody Project project);

    @PostMapping(value = "/save", produces = "application/json")
    Project save(@RequestBody Project project);

}