package ru.panasyuk.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class DBConst {

    @NotNull
    public static final String TABLE_PROJECT = "tm_project";

    @NotNull
    public static final String TABLE_TASK = "tm_task";

    @NotNull
    public static final String TABLE_USER = "tm_user";

    @NotNull
    public static final String TABLE_SESSION = "tm_session";

    @NotNull
    public static final String COLUMN_ID = "id";

    @NotNull
    public static final String COLUMN_CREATED = "created";

    @NotNull
    public static final String COLUMN_DATE_START = "date_start";

    @NotNull
    public static final String COLUMN_DATE_FINISH = "date_finish";

    @NotNull
    public static final String COLUMN_DESCRIPTION = "description";

    @NotNull
    public static final String COLUMN_EMAIL = "email";

    @NotNull
    public static final String COLUMN_FIRST_NAME = "first_name";

    @NotNull
    public static final String COLUMN_LAST_NAME = "last_name";

    @NotNull
    public static final String COLUMN_LOGIN = "login";

    @NotNull
    public static final String COLUMN_LOCKED = "locked";

    @NotNull
    public static final String COLUMN_MIDDLE_NAME = "middle_name";

    @NotNull
    public static final String COLUMN_NAME = "name";

    @NotNull
    public static final String COLUMN_PASSWORD = "password";

    @NotNull
    public static final String COLUMN_PROJECT_ID = "project_id";

    @NotNull
    public static final String COLUMN_ROLE = "role";

    @NotNull
    public static final String COLUMN_STATUS = "status";

    @NotNull
    public static final String COLUMN_USER_ID = "user_id";

    private DBConst() {
    }

}