package ru.panasyuk.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.panasyuk.tm.constant.DBConst;
import ru.panasyuk.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = DBConst.TABLE_PROJECT)
public final class Project {

    private static final long serialVersionUID = 1;

    @Id
    @NotNull
    @Column(name = DBConst.COLUMN_ID, length = 36, nullable = false, updatable = false)
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column(name = DBConst.COLUMN_NAME)
    private String name = "";

    @Nullable
    @Column(name = DBConst.COLUMN_DESCRIPTION)
    private String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = DBConst.COLUMN_STATUS, length = 30)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = DBConst.COLUMN_DATE_START, nullable = false)
    private Date dateStart = new Date();

    @Nullable
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = DBConst.COLUMN_DATE_FINISH)
    private Date dateFinish;

    public Project(@NotNull final String name) {
        this.name = name;
    }

}