package ru.panasyuk.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.panasyuk.tm.api.service.IProjectService;
import ru.panasyuk.tm.model.Project;
import ru.panasyuk.tm.repository.ProjectRepository;

import java.util.List;

@Service
public class ProjectService implements IProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Nullable
    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @NotNull
    @Override
    @Transactional
    public Project save(@NotNull final Project project) {
        return projectRepository.save(project);
    }

    @Override
    @Transactional
    public void saveAll(@NotNull final List<Project> projects) {
        projectRepository.saveAll(projects);
    }

    @Nullable
    @Override
    public Project findById(@Nullable final String id) {
        if (id == null) return null;
        return projectRepository.findById(id).orElse(null);
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return projectRepository.existsById(id);
    }

    @Override
    public long count() {
        return projectRepository.count();
    }

    @Transactional
    public void deleteById(@NotNull final String id) {
        projectRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void delete(@NotNull final Project project) {
        projectRepository.delete(project);
    }

    @Override
    @Transactional
    public void clear(@NotNull final List<Project> projects) {
        projectRepository.deleteAll(projects);
    }

    @Override
    public void clear() {
        projectRepository.deleteAll();
    }

}