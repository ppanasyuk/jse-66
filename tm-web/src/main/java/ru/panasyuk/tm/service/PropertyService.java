package ru.panasyuk.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.panasyuk.tm.api.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @Value("#{environment['database.dialect']}")
    private String dBDialect;

    @Value("#{environment['database.driver']}")
    private String dBDriver;

    @Value("#{environment['database.hbm2ddlauto']}")
    private String dBHbm2DdlAuto;

    @Value("#{environment['database.secondLevelCacheEnabled']}")
    private String dBSecondLevelCacheEnabled;

    @Value("#{environment['database.lazyLoadNoTrans']}")
    private String dBLazyLoadNoTransEnabled;

    @Value("#{environment['database.formatSql']}")
    private String dBFormatSql;

    @Value("#{environment['database.loggingEnabled']}")
    private Boolean dBLoggingEnabled;

    @Value("#{environment['database.cacheRegionFactory']}")
    private String dBCacheRegionFactory;

    @Value("#{environment['database.regionPrefix']}")
    private String dBCacheRegionPrefix;

    @Value("#{environment['database.configFilePath']}")
    private String dBConfigFilePath;

    @Value("#{environment['database.useQueryCache']}")
    private String dBUseQueryCache;

    @Value("#{environment['database.useMinPuts']}")
    private String dBUseMinimalPuts;

    @Value("#{environment['database.username']}")
    private String dBUser;

    @Value("#{environment['database.password']}")
    private String dBPassword;

    @Value("#{environment['database.url']}")
    private String dBUrl;

    @NotNull
    private static final String EMPTY_VALUE = "--//--";

}